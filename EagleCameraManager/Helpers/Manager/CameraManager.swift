//
//  CameraManager.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import Foundation

class CameraManager: NSObject {
    static let shared = CameraManager()
    
    var hasLoggedIn = false
    var refreshToken = ""
    var accessToken = ""
    
    var oauth:SignInModel? {
        didSet {
            accessToken = oauth?.accessToken ?? ""
            refreshToken = oauth?.refreshToken ?? ""
        }
    }
}
