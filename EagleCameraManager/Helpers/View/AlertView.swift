//
//  AlertView.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

class AlertView {
    
    static func show(in viewController: UIViewController, title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func showError(in viewController: UIViewController, message: String?) {
        let alert = UIAlertController(title: "Error", message: message ?? "There is something wrong. Please try again!", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
    
    static func show(in viewController: UIViewController, title: String?, message: String?, completion : @escaping () -> (Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Done", style: .default) { action in
            completion()
        })
        
        viewController.present(alert, animated: true, completion: nil)
    }
}

