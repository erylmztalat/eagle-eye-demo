//
//  TableViewCell.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

open class TableCell<T>: UITableViewCell {
    
    open var item: T? {
        didSet {
            layoutViews()
            configureViews(for: self.item)
        }
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func layoutViews() {}
    open func configureViews(for item: T?) {}
    
}

protocol SelfConfiguringCell {
    static var reuseIdentifier: String { get }
}
