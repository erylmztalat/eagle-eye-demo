//
//  FullScreenImageView.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

protocol FullScreenImageViewDelegate: class {
    func closedFullScreenImage()
}

class FullScreenImageView: UIView, UIScrollViewDelegate {
    
    weak var delegate:FullScreenImageViewDelegate?
        
    lazy var linkImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var linkScrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.black
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 9.0
        return scrollView
    }()
    
    lazy var closeButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "close_button"), for: .normal)
        return button
    }()
    
    var tapGesture = UITapGestureRecognizer()
    var panGesture = UIPanGestureRecognizer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        self.alpha = 0
        linkScrollView.delegate = self
        linkScrollView.frame = self.frame
        
        linkImageView.frame = self.frame
        self.addSubview(linkScrollView)
        linkScrollView.addSubview(linkImageView)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureToZoom))
        tapGesture.numberOfTapsRequired = 2
        linkScrollView.addGestureRecognizer(tapGesture)
        
        addGesture()
        
        closeButton.frame = CGRect(x: 10, y: 40, width: 30, height: 30)
        closeButton.addTarget(self, action: #selector(tappedCloseButton), for: .touchUpInside)
        self.addSubview(closeButton)
    }
    
    deinit {
        linkScrollView.removeGestureRecognizer(tapGesture)
        self.removeGestureRecognizer(panGesture)
    }
    
    func setImageView(imageData:Data) {
        linkImageView.image = UIImage(data: imageData, scale: 1.0)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return linkImageView
    }
    
    @objc func tapGestureToZoom(){
        if (self.linkScrollView.zoomScale > self.linkScrollView.minimumZoomScale) {
            self.linkScrollView.setZoomScale(self.linkScrollView.minimumZoomScale, animated: true)
        }else {
            self.linkScrollView.setZoomScale(self.linkScrollView.maximumZoomScale, animated: true)
        }
    }
    
    func addGesture(){
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(sender:)))
        self.addGestureRecognizer(panGesture)
    }
    
    @objc func respondToSwipeGesture(sender:UIPanGestureRecognizer) {
        if let direction = sender.direction {
            if direction.isVertical {
                self.closeView()
            }
        }
    }
    
    func closeView(){
        self.delegate?.closedFullScreenImage()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
    
    @objc func tappedCloseButton(){
        closeView()
    }
    
     required init?(coder aDecoder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
}
