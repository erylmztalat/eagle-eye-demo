//
//  Appearance.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

class Appearance {
    
    static func configure() {
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().barTintColor = UIColor(hex: "0087BE")
        UINavigationBar.appearance().tintColor = UIColor.white
    }
    
}

