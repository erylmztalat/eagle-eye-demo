//
//  NetworkRequest.swift
//  EagleCameraManager
//
//  Created by talate on 23.07.2021.
//

import Foundation
import Alamofire

let mainApiEndPoint = "http://rest.cameramanager.com/"

class NetworkRequest {
    
    //Oauth
    static let oauth = "oauth/token"
    
    //Cameras
    static let cameras = "rest/v2.4/cameras"
    static let cameraStatus = "/status"
    static let cameraImage = "/snapshot"
    
    static func request<T: Decodable> (
        method: HTTPMethod,
        parameters: Parameters?,
        headers:HTTPHeaders?,
        url: String,
        viewController: UIViewController,
        encoding: ParameterEncoding,
        responseModel: T.Type,
        showErrorAlerts: Bool,
        completion: @escaping (T?, Int) -> Void) {
        
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<600).responseDecodable(of: T.self) { response in
            
            switch response.result {
              case .success(let data):
                completion(data, response.response?.statusCode ?? 200)
              case .failure(let error):
                if showErrorAlerts {
                    AlertView.showError(in: viewController, message: error.errorDescription)
                }
                completion(nil, response.response?.statusCode ?? 400)
            }
        }
        
    }
    
    // MARK: SIGN IN API REQUEST
    static func signIn(username:String, password:String, viewcontroller: UIViewController, completionHandler: @escaping (SignInModel?) -> Void) {
        
        let url = mainApiEndPoint + oauth
        
        let params: Parameters = [
            "grant_type": "password",
            "scope": "write",
            "username": username,
            "password": password
        ]
        
        let user = "dev_test"
        let password = "3H1Bf6mCctIgpCuzvrnyekf3VhAUEnKJ"
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        let headers:HTTPHeaders = ["Authorization": "Basic \(base64Credentials)",
                                   "Accept": "application/json",
                                   "Content-Type": "application/json"]
        
        self.request(method: .post, parameters: params, headers: headers, url: url, viewController: viewcontroller, encoding: URLEncoding.queryString, responseModel: SignInModel.self, showErrorAlerts: true) { response,code  in
            completionHandler(response)
        }
    }
    
    // MARK: REFRESH TOKEN API REQUEST
    static func refreshToken(completionHandler: @escaping (Int) -> Void) {
        
        let url = mainApiEndPoint + oauth
        
        let params: Parameters = [
            "grant_type": "refresh_token",
            "scope": "write",
            "refresh_token": CameraManager.shared.refreshToken
        ]
        
        let user = "dev_test"
        let password = "3H1Bf6mCctIgpCuzvrnyekf3VhAUEnKJ"
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        let headers:HTTPHeaders = ["Authorization": "Basic \(base64Credentials)",
                                   "Accept": "application/json",
                                   "Content-Type": "application/json"]
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.queryString, headers: headers).validate(statusCode: 200..<600).responseDecodable(of: SignInModel.self) { response in
            if case let .success(data) = response.result {
                CameraManager.shared.oauth = data
                completionHandler(response.response?.statusCode ?? 200)
            }
        }
    }
    
    // MARK: CAMERAS API REQUEST
    static func getCameras(viewcontroller: UIViewController, completionHandler: @escaping ([CamerasModel]?) -> Void) {
        
        let url = mainApiEndPoint + cameras
        let token = CameraManager.shared.accessToken
        let headers:HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        self.request(method: .get, parameters: nil, headers: headers, url: url, viewController: viewcontroller, encoding: JSONEncoding.default, responseModel: [CamerasModel].self, showErrorAlerts: true) { response,code  in
            if code == 401 {
                self.refreshToken { code in
                    if code == 200 {
                        self.getCameras(viewcontroller: viewcontroller) { response in
                            completionHandler(response)
                        }
                    }
                }
            }else {
                completionHandler(response)
            }
        }
    }
    
    // MARK: CAMERA STATUS API REQUEST
    static func getCameraStatus(cameraID:Int, viewcontroller: UIViewController, completionHandler: @escaping (CameraStatusModel?) -> Void) {
        
        let url = mainApiEndPoint + cameras + "/\(cameraID)" + cameraStatus
        let token = CameraManager.shared.accessToken
        let headers:HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        self.request(method: .get, parameters: nil, headers: headers, url: url, viewController: viewcontroller, encoding: JSONEncoding.default, responseModel: CameraStatusModel.self, showErrorAlerts: true) { response,code  in
            if code == 401 {
                self.refreshToken { code in
                    if code == 200 {
                        self.getCameraStatus(cameraID: cameraID, viewcontroller: viewcontroller) { response in
                            completionHandler(response)
                        }
                    }
                }
            }else {
                completionHandler(response)
            }
        }
    }
    
    // MARK: CAMERA IMAGE API REQUEST
    static func getCameraImage(cameraID:Int, viewcontroller: UIViewController, completionHandler: @escaping (Data?) -> Void) {
        let params: Parameters = [
            "resolution": "320x240"
        ]
        let url = mainApiEndPoint + cameras + "/\(cameraID)" + cameraImage
        let token = CameraManager.shared.accessToken
        let headers:HTTPHeaders = ["Authorization": "Bearer \(token)"]
        AF.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).validate(statusCode: 200..<600).response { response in
            if case let .success(data) = response.result {
                completionHandler(data)
            }else {
                if response.response?.statusCode == 401 {
                    self.refreshToken { code in
                        if code == 200 {
                            self.getCameraImage(cameraID: cameraID, viewcontroller: viewcontroller) { response in
                                completionHandler(response)
                            }
                        }
                    }
                }
            }
        }
    }
    
}
