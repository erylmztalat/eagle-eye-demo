//
//  CameraCell.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

class CameraCell: TableCell<CamerasModel>, SelfConfiguringCell {
    static var reuseIdentifier: String = "CameraCellIdentifier"
    
    lazy var cameraLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.black.withAlphaComponent(0.7)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    override func layoutViews() {
        super.layoutViews()
        self.backgroundColor = UIColor.clear
        
        contentView.addSubview(cameraLabel)
        cameraLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        cameraLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        cameraLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
        cameraLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
    }
    
    override func configureViews(for item: CamerasModel?) {
        guard let item = item else { return }
        cameraLabel.text = item.name
    }
}
