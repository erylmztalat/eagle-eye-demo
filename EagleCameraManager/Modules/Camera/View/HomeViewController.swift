//
//  HomeViewController.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit
import Alamofire

class HomeViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var homeViewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        // Do any additional setup after loading the view.
    }

    func configure() {
        navigationItem.title = "Cameras"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CameraCell.self, forCellReuseIdentifier: CameraCell.reuseIdentifier)
        
        activityIndicator.startAnimating()
        
        homeViewModel.delegate = self
        homeViewModel.getCameras(vc: self)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(tappedLogOutButton))
    }

    @objc func tappedLogOutButton() {
        CameraManager.shared.oauth = nil
        CameraManager.shared.accessToken = ""
        CameraManager.shared.refreshToken = ""
        self.dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeViewModel.cameras.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CameraCell.reuseIdentifier) as? CameraCell {
            cell.item = homeViewModel.cameras[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cameraDetailVC = CameraDetailViewController()
        cameraDetailVC.camera = homeViewModel.cameras[indexPath.row]
        self.navigationController?.pushViewController(cameraDetailVC, animated: true)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func reloadCamerasData() {
        self.activityIndicator.stopAnimating()
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}
