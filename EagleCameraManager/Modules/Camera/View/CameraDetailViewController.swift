//
//  CameraDetailViewController.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import UIKit

class CameraDetailViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var cameraIdLabel: UILabel?
    @IBOutlet weak var deviceTypeIdLabel: UILabel?
    @IBOutlet weak var ethMacLabel: UILabel?
    @IBOutlet weak var zoneIdLabel: UILabel?
    @IBOutlet weak var accountIdLabel: UILabel?
    @IBOutlet weak var statusView: UIView?
    @IBOutlet weak var statusTypeView: UIView?
    @IBOutlet weak var statusLabel: UILabel?
    @IBOutlet weak var showCameraButton: UIButton?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var camera:CamerasModel?
    var viewModel = CameraDetailViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        nameLabel?.attributedText = NSMutableAttributedString().bold("Name: ").normal("\(camera?.name ?? "")")
        cameraIdLabel?.attributedText = NSMutableAttributedString().bold("Camera Id: ").normal("\(camera?.cameraId ?? 0)")
        deviceTypeIdLabel?.attributedText = NSMutableAttributedString().bold("Device Type Id: ").normal("\(camera?.deviceTypeId ?? 0)")
        ethMacLabel?.attributedText = NSMutableAttributedString().bold("Eth Mac Address: ").normal("\(camera?.ethMacAddress ?? "")")
        zoneIdLabel?.attributedText = NSMutableAttributedString().bold("Zone Id: ").normal("\(camera?.zoneId ?? 0)")
        accountIdLabel?.attributedText = NSMutableAttributedString().bold("Account Id: ").normal("\(camera?.accountId ?? 0)")
        
        statusTypeView?.layer.cornerRadius = 5
        viewModel.delegate = self
        viewModel.getCameraStatus(id: camera?.cameraId ?? 0, vc: self)
        
        showCameraButton?.layer.cornerRadius = 5
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func tappedShowCameraButton(_ sender: Any) {
        activityIndicator.startAnimating()
        viewModel.getCameraImage(id: camera?.cameraId ?? 0, vc: self)
    }

}

extension CameraDetailViewController: CameraDetailViewModelDelegate {
    func reloadCameraImage() {
        activityIndicator.stopAnimating()
        guard let data = viewModel.imageData else { return }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let fullScreenImageView = FullScreenImageView(frame: self.view.bounds)
        fullScreenImageView.delegate = self
        fullScreenImageView.setImageView(imageData: data)
        self.view.addSubview(fullScreenImageView)
        UIView.animate(withDuration: 0.15) { [weak self] in
            fullScreenImageView.alpha = 1.0
        }
    }
    
    func reloadCameraStatus() {
        DispatchQueue.main.async { [weak self] in
            guard let status = self?.viewModel.cameraStatus else {
                self?.statusView?.isHidden = true
                return
            }
            self?.statusView?.isHidden = false
            if status.online ?? false {
                self?.statusTypeView?.backgroundColor = UIColor(hex: "27A720")
                self?.statusLabel?.text = "Online"
            }else {
                self?.statusTypeView?.backgroundColor = UIColor(hex: "A73131")
                self?.statusLabel?.text = "Offline"
            }
        }
    }
}

extension CameraDetailViewController: FullScreenImageViewDelegate {
    func closedFullScreenImage() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
