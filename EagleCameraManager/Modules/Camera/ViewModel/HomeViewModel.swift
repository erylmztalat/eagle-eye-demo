//
//  HomeViewModel.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import Foundation

protocol HomeViewModelDelegate: class {
    func reloadCamerasData()
}

class HomeViewModel {
    weak var delegate:HomeViewModelDelegate?
    var cameras:[CamerasModel] = []
    func getCameras(vc:BaseViewController) {
        NetworkRequest.getCameras(viewcontroller: vc) { [weak self] response in
            guard let resp = response else {
                return
            }
            self?.cameras = resp
            self?.delegate?.reloadCamerasData()
        }
    }
}
