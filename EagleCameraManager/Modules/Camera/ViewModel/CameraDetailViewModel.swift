//
//  CameraDetailViewModel.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import Foundation

protocol CameraDetailViewModelDelegate: class {
    func reloadCameraStatus()
    func reloadCameraImage()
}
class CameraDetailViewModel {
    var cameraStatus:CameraStatusModel?
    var imageData:Data?
    weak var delegate:CameraDetailViewModelDelegate?
    
    func getCameraStatus(id:Int, vc:BaseViewController) {
        NetworkRequest.getCameraStatus(cameraID: id, viewcontroller: vc) { [weak self] response in
            self?.cameraStatus = response
            self?.delegate?.reloadCameraStatus()
        }
    }
    
    func getCameraImage(id:Int, vc:BaseViewController) {
        NetworkRequest.getCameraImage(cameraID: id, viewcontroller: vc) { [weak self] response in
            self?.imageData = response
            self?.delegate?.reloadCameraImage()
        }
    }
}
