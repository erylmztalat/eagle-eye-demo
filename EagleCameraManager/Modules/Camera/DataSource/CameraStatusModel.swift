//
//  CameraStatusModel.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import Foundation

struct CameraStatusModel: Decodable {
    
    let cameraId:Int?
    let online:Bool?
    let recordingOnCloud:Bool?
    let recordingOnSd:Bool?
    let audioEnabled:Bool?
    let passwordKnown:Bool?
    let passwordStatus:String?
    let firmwareStatus:String?
    let connectionType:String?
    let lastConnectionResult:String?
    
    enum CodingKeys: String, CodingKey {
      case cameraId = "cameraId"
      case online = "online"
      case recordingOnCloud = "recordingOnCloud"
      case recordingOnSd = "recordingOnSd"
      case audioEnabled = "audioEnabled"
      case passwordKnown = "passwordKnown"
      case passwordStatus = "passwordStatus"
      case firmwareStatus = "firmwareStatus"
      case connectionType = "connectionType"
      case lastConnectionResult = "lastConnectionResult"
    }
}
