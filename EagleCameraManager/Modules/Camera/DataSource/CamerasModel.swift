//
//  CamerasModel.swift
//  EagleCameraManager
//
//  Created by talate on 23.07.2021.
//

import Foundation

struct CamerasModel: Decodable {
    
    let cameraId:Int?
    let name:String?
    let deviceTypeId:Int?
    let ethMacAddress:String?
    let zoneId:Int?
    let accountId:Int?
    
    enum CodingKeys: String, CodingKey {
      case cameraId = "cameraId"
      case name = "name"
      case deviceTypeId = "deviceTypeId"
      case ethMacAddress = "ethMacAddress"
      case zoneId = "zoneId"
      case accountId = "accountId"
    }
}
