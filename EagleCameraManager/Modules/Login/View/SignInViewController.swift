//
//  SignInViewController.swift
//  EagleCameraManager
//
//  Created by talate on 23.07.2021.
//

import UIKit

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var signInViewModel = SignInViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    func configure() {
        signInViewModel.delegate = self
        signInButton.layer.cornerRadius = 5
        
        usernameTextField.layer.borderColor = UIColor.black.withAlphaComponent(0.25).cgColor
        usernameTextField.layer.borderWidth = 0.25
        usernameTextField.layer.cornerRadius = 5
        usernameTextField.attributedPlaceholder = NSAttributedString(string: usernameTextField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black.withAlphaComponent(0.25)])
        usernameTextField.setLeftPaddingPoints(5)
        passwordTextField.layer.borderColor = UIColor.black.withAlphaComponent(0.25).cgColor
        passwordTextField.layer.borderWidth = 0.25
        passwordTextField.layer.cornerRadius = 5
        passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black.withAlphaComponent(0.25)])
        passwordTextField.setLeftPaddingPoints(5)
    }

    @IBAction func tappedSignInButton(_ sender: Any) {
        guard let username = usernameTextField.text , let password = passwordTextField.text else {
            AlertView.showError(in: self, message: "Form is not valid")
            return
        }
        activityIndicator.startAnimating()
        signInViewModel.signIn(with: username, password: password, vc: self)
    }
}

extension SignInViewController: SignInViewModelDelegate {
    func hasLoggedIn(with success: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            if success {
                self?.usernameTextField.text = ""
                self?.passwordTextField.text = ""
                let homeNavigationController = UINavigationController(rootViewController: HomeViewController())
                homeNavigationController.modalPresentationStyle = .custom
                self?.present(homeNavigationController, animated: true)
            }
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
