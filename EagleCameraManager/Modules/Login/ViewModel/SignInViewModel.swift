//
//  SignInViewModel.swift
//  EagleCameraManager
//
//  Created by talate on 24.07.2021.
//

import Foundation

protocol SignInViewModelDelegate: class {
    func hasLoggedIn(with success:Bool)
}

class SignInViewModel {
    weak var delegate:SignInViewModelDelegate?
    func signIn(with username:String, password:String, vc:BaseViewController) {
        NetworkRequest.signIn(username: username, password: password, viewcontroller: vc) { [weak self] response in
            guard let resp = response else {
                self?.delegate?.hasLoggedIn(with: false)
                return
            }
            CameraManager.shared.oauth = resp
            CameraManager.shared.hasLoggedIn = true
            self?.delegate?.hasLoggedIn(with: true)
        }
    }
}
