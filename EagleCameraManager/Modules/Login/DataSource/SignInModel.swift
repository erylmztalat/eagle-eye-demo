//
//  SignInModel.swift
//  EagleCameraManager
//
//  Created by talate on 23.07.2021.
//

import Foundation

struct SignInModel: Decodable {
    
    let accessToken:String?
    let tokenType:String?
    let refreshToken:String?
    let expiresIn:Int?
    let scope:String?
    
    enum CodingKeys: String, CodingKey {
      case accessToken = "access_token"
      case tokenType = "token_type"
      case refreshToken = "refresh_token"
      case expiresIn = "expires_in"
      case scope = "scope"
    }
}
